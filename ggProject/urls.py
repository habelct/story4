from django.urls import path
from . import views

urlpatterns = [
    path('homepage/', views.home, name = 'homepage'),
    path('contact/', views.contact, name = 'contact'),
    path('experiences/', views.experiences, name = 'experiences'),
    path('education/', views.education, name = 'education'),
    path('about/', views.about, name = 'about')
    
]