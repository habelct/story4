from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'homepage.html')

def experiences(request):
    return render(request, 'experiences.html')

def about(request):
    return render(request, 'about.html')

def contact(request):
    return render(request, 'contact.html') 

def education(request):
    return render(request, 'education.html')   